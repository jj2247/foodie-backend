-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 27, 2022 at 12:34 AM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mydb`
--

-- --------------------------------------------------------

--
-- Table structure for table `hangout`
--

CREATE TABLE `hangout` (
  `id` int(11) NOT NULL,
  `datetime` datetime DEFAULT NULL,
  `Restaurant_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `hangout_has_user`
--

CREATE TABLE `hangout_has_user` (
  `Hangout_id` int(11) NOT NULL,
  `User_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `restaurant`
--

CREATE TABLE `restaurant` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `capacity` int(11) NOT NULL,
  `location` varchar(45) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `review` decimal(3,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `restaurant`
--

INSERT INTO `restaurant` (`id`, `name`, `capacity`, `location`, `description`, `review`) VALUES
(1, 'Roza Slon Bežigrad\r\n', 20, 'Dunajska c. 115', '« Sveže, okusno, zdravo in cenovno ugodno « je naš moto kuhanja. Pri pripravi Tajskih jedi uporabljamo originalne sestavine iz Tajske.', '9.20'),
(2, 'Kavarna Tiskarna	 ', 25, 'Kranjčeva u. 22', 'An old printing house that became the best burger place in town', '9.00'),
(3, 'Cantina Mexicana', 50, 'Knafljev prehod 2', 'Delicious genuine Mexican dishes', '9.00'),
(4, 'McDonald\'s Aleja', 60, 'Rakuševa ulica 1', 'McDonald\'s is one of the most recognizable brands in the world and in our country, which has become synonymous with fast and quality food and beverage service. Since the end of 1993, when we opened our first restaurant on Čopova street in Ljubljana, we ha', '8.80'),
(5, 'Dobra Vila', 28, 'Celovška c. 166', 'Great pizzas made with a smile', '8.80'),
(6, 'Novi Šang Hai', 55, 'Kapiteljska u. 5', 'All your favorite Chinese dishes, authentic & delicious', '9.20'),
(7, 'Dodo Pizza', 20, 'Ilirska u. 4', 'Quickly expanding international pizza chain with tasty, crunchy and wholesome pizza', '9.30'),
(8, 'Kitajski Dvor', 70, 'Dunajska c. 29', 'Original Chinese specialities', '8.80'),
(9, 'Das ist Valter', 58, 'Njegoševa cesta 10', 'Real traditional Bosnian specialties', '9.40'),
(10, 'Rex Bistro', 18, 'Cankarjeva c. 6', 'Fuck rex bistro jadniki ne plačujejo delavcev, pa pritožujejo se čez študente ', '5.10'),
(11, 'Maharaja', 30, 'Vodnikova c. 35', 'The Maharaja offers dishes, real delicacies prepared by masters of Indian cuisine', '9.40'),
(12, 'Barabella Juicebar', 15, 'Slovenska c. 38', 'Delicous plant-based cuisine for everyday. Specialty are yummy desserts :D', '8.70');

-- --------------------------------------------------------

--
-- Table structure for table `restaurant_has_type`
--

CREATE TABLE `restaurant_has_type` (
  `restaurant_id` int(11) NOT NULL,
  `types_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `restaurant_has_type`
--

INSERT INTO `restaurant_has_type` (`restaurant_id`, `types_id`) VALUES
(1, 1),
(1, 12),
(2, 1),
(2, 7),
(2, 10),
(3, 2),
(3, 4),
(4, 1),
(4, 6),
(4, 11),
(5, 3),
(5, 10),
(6, 13),
(7, 1),
(7, 3),
(8, 13),
(9, 5),
(9, 10),
(9, 11),
(10, 3),
(10, 11),
(11, 9),
(11, 12),
(12, 5),
(12, 6),
(12, 7);

-- --------------------------------------------------------

--
-- Table structure for table `type`
--

CREATE TABLE `type` (
  `id` int(11) NOT NULL,
  `type` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `type`
--

INSERT INTO `type` (`id`, `type`) VALUES
(1, 'FAST_FOOD'),
(2, 'GRILL'),
(3, 'ITALIAN'),
(4, 'MEXICAN'),
(5, 'SLOVENIAN'),
(6, 'DESSERT'),
(7, 'DRINKS'),
(8, 'CAFE'),
(9, 'INDIAN'),
(10, 'STREET_FOOD'),
(11, 'STUDENT'),
(12, 'ASIAN'),
(13, 'CHINESE');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `first_name` varchar(45) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `date_of_birth` varchar(45) NOT NULL,
  `gender` int(11) NOT NULL,
  `email` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `occupation` varchar(45) DEFAULT NULL,
  `social` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `hangout`
--
ALTER TABLE `hangout`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_Hangout_Restaurant1_idx` (`Restaurant_id`);

--
-- Indexes for table `hangout_has_user`
--
ALTER TABLE `hangout_has_user`
  ADD PRIMARY KEY (`Hangout_id`,`User_id`),
  ADD KEY `fk_Hangout_has_User_User1_idx` (`User_id`),
  ADD KEY `fk_Hangout_has_User_Hangout1_idx` (`Hangout_id`);

--
-- Indexes for table `restaurant`
--
ALTER TABLE `restaurant`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `restaurant_has_type`
--
ALTER TABLE `restaurant_has_type`
  ADD PRIMARY KEY (`restaurant_id`,`types_id`),
  ADD KEY `fk_Restaurant_has_Type_Type1_idx` (`types_id`),
  ADD KEY `fk_Restaurant_has_Type_Restaurant_idx` (`restaurant_id`);

--
-- Indexes for table `type`
--
ALTER TABLE `type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `hangout`
--
ALTER TABLE `hangout`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `restaurant`
--
ALTER TABLE `restaurant`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `type`
--
ALTER TABLE `type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `hangout`
--
ALTER TABLE `hangout`
  ADD CONSTRAINT `fk_Hangout_Restaurant1` FOREIGN KEY (`Restaurant_id`) REFERENCES `restaurant` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `hangout_has_user`
--
ALTER TABLE `hangout_has_user`
  ADD CONSTRAINT `fk_Hangout_has_User_Hangout1` FOREIGN KEY (`Hangout_id`) REFERENCES `hangout` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Hangout_has_User_User1` FOREIGN KEY (`User_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `restaurant_has_type`
--
ALTER TABLE `restaurant_has_type`
  ADD CONSTRAINT `fk_Restaurant_has_Type_Restaurant` FOREIGN KEY (`Restaurant_id`) REFERENCES `restaurant` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Restaurant_has_Type_Type1` FOREIGN KEY (`types_id`) REFERENCES `type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
