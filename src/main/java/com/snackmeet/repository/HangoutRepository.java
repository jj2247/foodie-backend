package com.snackmeet.repository;

import com.snackmeet.model.Hangout;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface HangoutRepository extends JpaRepository<Hangout, Long> {
    List<Hangout> findAllByRestaurantId(Long restaurantId);

    List<Hangout> findByUsers_id(Long userId);
}