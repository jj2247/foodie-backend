package com.snackmeet.repository;

import com.snackmeet.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
    User getUserByPasswordAndEmail(String password,String email);
}