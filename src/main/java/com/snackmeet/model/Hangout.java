package com.snackmeet.model;

import jakarta.persistence.*;

import java.util.Set;

@Entity
@Table(name = "hangout")
public class Hangout {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String datetime;
    @OneToOne
    @JoinColumn(
            name = "Restaurant_id",
            nullable = false
    )
    private Restaurant restaurant;
    @ManyToMany
    @JoinTable(
            name = "hangout_has_user",
            joinColumns = @JoinColumn(name = "hangout_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id")

    )
    private Set<User> users;

    public Hangout(){}

    public Hangout(Long id, String datetime, Restaurant restaurant, Set<User> users) {
        this.id = id;
        this.datetime = datetime;
        this.restaurant = restaurant;
        this.users = users;
    }

    public Long getId() {
        return id;
    }

    public String getDatetime() {
        return datetime;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    public Set<User> getUsers() {
        return users;
    }
}
