package com.snackmeet.model;

import com.snackmeet.constant.Restaurant_Type;
import jakarta.persistence.*;


@Entity(name = "type")
@Table(name = "type")
public class Type {
    @Id
    @GeneratedValue(strategy =  GenerationType.IDENTITY)
    private Long id;
    private Restaurant_Type Type;

    public Type(){}

    public Type(Long id,Restaurant_Type type) {
        this.id = id;
        this.Type = type;
    }

    public Restaurant_Type getType() {
        return Type;
    }

    public Long getId() {
        return id;
    }
}
