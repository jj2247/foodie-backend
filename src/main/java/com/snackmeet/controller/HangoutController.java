package com.snackmeet.controller;

import com.snackmeet.model.Hangout;
import com.snackmeet.model.Restaurant;
import com.snackmeet.model.User;
import com.snackmeet.repository.HangoutRepository;
import com.snackmeet.repository.RestaurantRepository;
import com.snackmeet.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.stereotype.Controller;


import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Controller
public class HangoutController {
    @Autowired
    private HangoutRepository hangoutRepository;

    @Autowired
    private RestaurantRepository restaurantRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    public HangoutController(HangoutRepository hangoutRepository, RestaurantRepository restaurantRepository, UserRepository userRepository){
        this.hangoutRepository = hangoutRepository;
        this.restaurantRepository = restaurantRepository;
        this.userRepository = userRepository;
    }

    @QueryMapping
    public List<Hangout> restaurantHangouts(@Argument Long restaurantId){
        return hangoutRepository.findAllByRestaurantId(restaurantId);
    }

    @QueryMapping
    public List<Hangout> userHangouts(@Argument Long userId){
        return hangoutRepository.findByUsers_id(userId);
    }

    @MutationMapping
    public boolean createHangout(@Argument Long restaurantId, @Argument Long userId,
                                 @Argument String datetime){
        Hangout hangout = new Hangout();
        Restaurant res = restaurantRepository.findById(restaurantId).orElse(new Restaurant());
        if(res.getId() == null){
            return false;
        }

        User usr = userRepository.findById(userId).orElse(new User());
        if(usr.getId() == null){
            return false;
        }

        hangout.setDatetime(datetime);
        hangout.setRestaurant(res);
        Set<User> seznam = new HashSet<>();
        seznam.add(usr);
        hangout.setUsers(seznam);
        hangoutRepository.save(hangout);
        return true;
    }

    @MutationMapping
    public boolean joinHangout(@Argument Long hangoutId, @Argument Long userId){
        User usr = userRepository.findById(userId).orElse(new User());
        Hangout hangoutToUpdate = hangoutRepository.findById(hangoutId).orElse(new Hangout());

        if(usr.getId() == null || hangoutToUpdate.getId() == null){
            return false;
        }
        Set<User> newSet = hangoutToUpdate.getUsers();
        newSet.add(usr);
        hangoutToUpdate.setUsers(newSet);
        hangoutRepository.save(hangoutToUpdate);
        return true;
    }

    @MutationMapping
    public boolean leaveHangout(@Argument Long hangoutId, @Argument Long userId){
        Hangout hangoutToUpdate = hangoutRepository.findById(hangoutId).orElse(new Hangout());
        User usr = userRepository.findById(userId).orElse(new User());
        if(usr.getId() == null || hangoutToUpdate.getId() == null){
            return false;
        }
        Set<User> joined = hangoutToUpdate.getUsers();
        joined.remove(usr);
        if(joined.size() == 0) {
            hangoutRepository.delete(hangoutToUpdate);
            return true;
        } else {
            hangoutToUpdate.setUsers(joined);
            hangoutRepository.save(hangoutToUpdate);
            return true;
        }
    }
}
