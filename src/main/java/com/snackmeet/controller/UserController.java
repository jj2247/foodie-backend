package com.snackmeet.controller;

import com.snackmeet.constant.Gender;
import com.snackmeet.constant.Social;
import com.snackmeet.model.User;
import com.snackmeet.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.stereotype.Controller;

@Controller
public class UserController {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    public UserController(UserRepository userRepository){
        this.userRepository = userRepository;
    }

    @MutationMapping
    public Boolean register(@Argument String firstName,@Argument String lastName,@Argument String dateOfBirth,
                            @Argument Gender gender, @Argument String email, @Argument String password,
                            @Argument String occupation,@Argument Social social){
        User usr = new User();
        usr.setFirstName(firstName);
        usr.setLastName(lastName);
        usr.setGender(gender);
        usr.setPassword(password);
        usr.setDateOfBirth(dateOfBirth);
        usr.setEmail(email);
        usr.setOccupation(occupation);
        usr.setSocial(social);
        userRepository.save(usr);
        return true;
    }

    @QueryMapping
    public User login(@Argument String email, @Argument String password){
        return userRepository.getUserByPasswordAndEmail(password,email);
    }
}
