package com.snackmeet.controller;

import com.snackmeet.model.Restaurant;
import com.snackmeet.repository.RestaurantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.stereotype.Controller;


import java.util.List;

@Controller
public class RestaurantController {
    @Autowired
    private RestaurantRepository restaurantRepository;

    @Autowired
    public RestaurantController(RestaurantRepository restaurantRepository){
        this.restaurantRepository = restaurantRepository;
    }

    @QueryMapping
    public List<Restaurant> restaurants(){
        return restaurantRepository.findAll();
    }
}
