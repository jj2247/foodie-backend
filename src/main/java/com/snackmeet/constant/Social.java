package com.snackmeet.constant;

public enum Social {
    INTROVERTED,
    EXTROVERTED,
    INBETWEEN
}
