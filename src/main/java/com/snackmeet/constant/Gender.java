package com.snackmeet.constant;

public enum Gender {
    MALE,
    FEMALE
}
