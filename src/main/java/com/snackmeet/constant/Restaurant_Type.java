package com.snackmeet.constant;

public enum Restaurant_Type {
    FAST_FOOD,
    GRILL,
    ITALIAN,
    MEXICAN,
    SLOVENIAN,
    DESSERT,
    DRINKS,
    CAFE,
    INDIAN,
    STREET_FOOD,
    STUDENT
}
