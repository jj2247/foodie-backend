# Snackmeet Backend

## Description
Backend for Snackmeet application build with Java, Graphql, Springboot

## Usage
For the backend to work correctly you need to specify the `URL`, `username` and `password` in `resources/application.properties` which tells the application to which mysql database it should connect to. Need to also make sure that a database with the name `mydb` exists.
```bash
spring.datasource.url=<mysql-server-url>
spring.datasource.username=<username>
spring.datasource.password=<password>
```

## Authors and acknowledgment
Nermin Mujagić, Nika Škerjanec, Jaša Jovan

## References
- [Mutation example (ta link verjetn najbuls)](https://techdozo.dev/spring-for-graphql-mutation/)
  - [Github link](https://github.com/techdozo/graphql-spring-mutation/tree/master/src/main/java/dev/techdozo/graphql)
- [Spring query example](https://www.graphql-java.com/tutorials/getting-started-with-spring-boot/)
- [Spring for Graphql docs](https://docs.spring.io/spring-graphql/docs/current/reference/html/#overview)